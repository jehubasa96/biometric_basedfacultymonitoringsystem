VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form Form1 
   Caption         =   "e-Gizmo Fingerprint Scanner Demo"
   ClientHeight    =   4875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8805
   LinkTopic       =   "Form1"
   ScaleHeight     =   4875
   ScaleWidth      =   8805
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "Cancel"
      Height          =   495
      Left            =   6600
      TabIndex        =   8
      Top             =   3360
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   1455
      Left            =   240
      TabIndex        =   3
      Top             =   1800
      Width           =   8295
      Begin VB.CommandButton cmdClear 
         Caption         =   "Remove All Users"
         Height          =   735
         Left            =   6360
         TabIndex        =   7
         Top             =   480
         Width           =   975
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "Remove"
         Height          =   375
         Left            =   360
         TabIndex        =   6
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmdReg 
         Caption         =   "Enroll"
         Height          =   375
         Left            =   360
         TabIndex        =   5
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox txtUserNo 
         Alignment       =   2  'Center
         Height          =   495
         Left            =   1800
         TabIndex        =   4
         Text            =   "1"
         Top             =   600
         Width           =   615
      End
   End
   Begin VB.TextBox txtResults 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   2640
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   360
      Width           =   5895
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   3120
      Top             =   4320
   End
   Begin VB.CommandButton cmdRead 
      Caption         =   "Read Mode"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   360
      Width           =   1215
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   480
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   3
      DTREnable       =   -1  'True
      RThreshold      =   24
      BaudRate        =   115200
   End
   Begin VB.TextBox txtWindow 
      Height          =   495
      Left            =   4200
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   4200
      Width           =   4335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SData(24) As Byte
Dim SReply(48) As Byte
Dim Replied As Boolean
Dim Rx As String
Dim CKS As Integer

Private Sub cmdClear_Click()
Dim i As Integer
Dim dummy As String

For i = 1 To 22
    SData(i) = 0
Next i

SData(0) = &H55
SData(1) = &HAA
SData(2) = &H6
SData(3) = &H1
Call Checksum
Replied = False
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
While Replied = False
    DoEvents
    Wend
If SReply(6) = 0 Then
    txtResults.Text = "All Users deleted!"
Else
    txtResults.Text = "Error!"
End If
End Sub

Private Sub cmdRead_Click()
Dim i As Integer
Dim dummy As String
txtResults.Text = "Lagay mo daliri mo (isa lang!).."
  
For i = 1 To 22
    SData(i) = 0
Next i
SData(0) = &H55
SData(1) = &HAA
SData(2) = &H2
SData(3) = &H1
Call Checksum
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
back:
Replied = False
While Replied = False
    DoEvents
    Wend
If Asc(Mid$(Right$(Rx, 24), 7, 1)) = 0 Then
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &HF4 And Asc(Mid$(Right$(Rx, 24), 10, 1)) = &HFF Then
        GoTo back
    End If
    txtResults.Text = "Hello No. " + Str(Int(Asc(Mid$(Right$(Rx, 24), 10, 1)) * 256 + Asc(Mid$(Right$(Rx, 24), 9, 1)))) + "  Welcome!"
Else
    Print_Error
End If
    
End Sub

Private Sub cmdReg_Click()
Dim i As Integer
Dim dummy As String
Dim V1, V2 As Integer
Dim Good As Boolean

Good = True

V1 = Int(Val(txtUserNo.Text) / 256)
V2 = Val(txtUserNo.Text) - V1 * 256

For i = 1 To 22
    SData(i) = 0
Next i
SData(0) = &H55
SData(1) = &HAA
SData(2) = &H3
SData(3) = &H1
SData(4) = &H2
SData(5) = &H0
SData(6) = V2
SData(7) = V1
Call Checksum
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
back:
Replied = False
While Replied = False
    DoEvents
    Wend
If SReply(6) = 0 Then
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &HF1 And Asc(Mid$(Right$(Rx, 24), 10, 1)) = &HFF Then
        txtResults.Text = "Press a finger to register first time."
    End If
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &HF2 And Asc(Mid$(Right$(Rx, 24), 10, 1)) = &HFF Then
        txtResults.Text = "Press the same finger the second time..."
    End If
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &HF3 And Asc(Mid$(Right$(Rx, 24), 10, 1)) = &HFF Then
        txtResults.Text = "Press the same finger the last time..."
    End If
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &HF4 And Asc(Mid$(Right$(Rx, 24), 10, 1)) = &HFF Then
        txtResults.Text = "Lift finger..."
    End If
    If Asc(Mid$(Right$(Rx, 24), 5, 1)) = &H6 Then
        txtResults.Text = "Ok"
        Exit Sub
    End If
    GoTo back
Else
    Print_Error
    If SReply(8) = &H21 Then GoTo back
    End If
End Sub

Private Sub cmdRemove_Click()
Dim i As Integer
Dim dummy As String
Dim V1, V2 As Integer

V1 = Int(Val(txtUserNo.Text) / 256)
V2 = Val(txtUserNo.Text) - V1 * 256

For i = 1 To 22
    SData(i) = 0
Next i

SData(0) = &H55
SData(1) = &HAA
SData(2) = &H5
SData(3) = &H1
SData(4) = &H2
SData(5) = &H0
SData(6) = V2
SData(7) = V1
Call Checksum
Replied = False
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
While Replied = False
    DoEvents
    Wend
If SReply(6) = 0 Then
    txtResults.Text = "User No. " + txtUserNo.Text + " Removed!"
Else
    txtResults.Text = "Error!"
End If

End Sub

Private Sub Command1_Click()
Dim i As Integer
Dim dummy As String
Dim V1, V2 As Integer

V1 = Int(Val(txtUserNo.Text) / 256)
V2 = Val(txtUserNo.Text) - V1 * 256

For i = 1 To 22
    SData(i) = 0
Next i

SData(0) = &H55
SData(1) = &HAA
SData(2) = &H4
SData(3) = &H1
SData(4) = &H2
SData(5) = &H0
SData(6) = V2
SData(7) = V1
Call Checksum
Replied = False
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
While Replied = False
    DoEvents
    Wend

End Sub

Private Sub Command2_Click()
Dim i As Integer
Dim dummy As String
Dim V1, V2 As Integer

V1 = Int(Val(txtUserNo.Text) / 256)
V2 = Val(txtUserNo.Text) - V1 * 256

For i = 1 To 22
    SData(i) = 0
Next i

SData(0) = &H55
SData(1) = &HAA
SData(2) = &H2
SData(3) = &H1
'SData(4) = &H2
'SData(6) = V1
'SData(7) = V2
Call Checksum
Replied = False
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
While Replied = False
    DoEvents
    Wend
End Sub

Private Sub Command3_Click()
Dim i As Integer
Dim dummy As String

For i = 1 To 22
    SData(i) = 0
Next i

SData(0) = &H55
SData(1) = &HAA
SData(2) = &H30
SData(3) = &H1
Call Checksum
Replied = False
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
While Replied = False
    DoEvents
    Wend
If SReply(6) = 0 Then
    txtResults.Text = "Cancelled!"
Else
    txtResults.Text = "Error!"
End If

End Sub

Private Sub Command4_Click()
    LED_On
End Sub

Private Sub Command5_Click()
    LED_Off
End Sub

Private Sub Form_Load()
MSComm1.PortOpen = True
End Sub

Public Sub Checksum()
Dim i As Integer

CKS = 0
For i = 0 To 21
    CKS = CKS + SData(i)
Next i

SData(22) = CKS And &HFF
SData(23) = (CKS - (CKS And &HFF)) / 256

End Sub

Public Sub Finger_Detect()
Dim i As Integer
Dim dummy As String

wait_finger:
For i = 1 To 22
    SData(i) = 0
Next i
SData(0) = &H55
SData(1) = &HAA
SData(2) = &H13
SData(3) = &H1
Call Checksum
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
Replied = False
While Replied = False
    DoEvents
    Wend
If SReply(8) = 0 Then
    GoTo wait_finger
End If

End Sub

Public Sub LED_On()
Dim i As Integer
Dim dummy As String

For i = 1 To 22
    SData(i) = 0
Next i
SData(0) = &H55
SData(1) = &HAA
SData(2) = &H24
SData(3) = &H1
SData(4) = 2
SData(5) = 0
SData(6) = 1
Call Checksum
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
Replied = False
While Replied = False
    DoEvents
    Wend

End Sub

Public Sub LED_Off()
Dim i As Integer
Dim dummy As String

For i = 1 To 22
    SData(i) = 0
Next i
SData(0) = &H55
SData(1) = &HAA
SData(2) = &H24
SData(3) = &H1
SData(4) = 2
Call Checksum
dummy = MSComm1.Input
For i = 0 To 23
MSComm1.Output = Chr(SData(i))
Next i
Replied = False
While Replied = False
    DoEvents
    Wend
End Sub

Public Sub Print_Error()
    If Asc(Mid$(Right$(Rx, 24), 9, 1)) = &H12 Then
        txtResults.Text = "Di kita kilala! Tsupi!"
    ElseIf Asc(Mid$(Right$(Rx, 24), 9, 1)) = &H14 Then
        txtResults.Text = "Location not empty!"
    ElseIf Asc(Mid$(Right$(Rx, 24), 9, 1)) = &H15 Then
        txtResults.Text = "No enrolled user!"
    ElseIf Asc(Mid$(Right$(Rx, 24), 9, 1)) = &H21 Then
        txtResults.Text = "Bad quality fingerprint image!"
    ElseIf Asc(Mid$(Right$(Rx, 24), 9, 1)) = &H60 Then
        txtResults.Text = "Location No. not valid!"
    End If
End Sub

Private Sub MSComm1_OnComm()
Dim RxNumeric As String
Dim i As Integer

Rx = MSComm1.Input
RxNumeric = ""
For i = 1 To Len(Rx)
SReply(i - 1) = Asc(Mid(Rx, i, 1))
RxNumeric = RxNumeric + " " + Hex(SReply(i - 1))
Next i
txtWindow.Text = RxNumeric
Replied = True
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
End Sub
