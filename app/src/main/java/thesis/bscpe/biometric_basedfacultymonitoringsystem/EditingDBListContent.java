package thesis.bscpe.biometric_basedfacultymonitoringsystem;

/**
 * This class is one of the requirements of Recyclerview
 */

public class EditingDBListContent {
    String FNAME, MNAME, LASTNAME, SCHED;
    byte[] image;
    int id;

    public EditingDBListContent(int id, String FNAME, String MNAME, String LASTNAME, String SCHED,
                                byte[] image) {
        this.id = id;
        this.FNAME = FNAME;
        this.MNAME = MNAME;
        this.LASTNAME = LASTNAME;
        this.SCHED = SCHED;
        this.image = image;
    }
}
