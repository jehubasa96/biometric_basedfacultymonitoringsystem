package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

public class AdminEditingPage extends AppCompatActivity implements AdminEditingRecyclerAdapter.OnItemClick {

    private DatabaseHelper db;
    private RecyclerView recycler;
    private AdminEditingRecyclerAdapter adapter;
    private LinearLayoutManager layoutmanager;
    private Toolbar toolbar;
    private ImageButton returnButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Code for fullscreen Mode
        getWindow().addFlags(WindowManager.LayoutParams
                .FLAG_FULLSCREEN);

        setContentView(R.layout.activity_admin_editing_page);

        //ToolBar Code
        toolbar = (Toolbar) findViewById(R.id.toolbarAdminEdit);
        returnButton = (ImageButton) toolbar.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AdminEditingPage.this, AdminPanel.class);
                startActivity(i);
                finish();
            }
        });

        //Recycler View Codes
        db = new DatabaseHelper(this);
        recycler = (RecyclerView) findViewById(R.id.editRecycler);
        adapter = new AdminEditingRecyclerAdapter(this, db.getData());
        adapter.setOnItemClick(this);
        recycler.setAdapter(adapter);
        layoutmanager = new GridLayoutManager(this, 3);
        recycler.setLayoutManager(layoutmanager);
    }


    @Override
    public void item(int id, String f, String m, String l, byte[] img, String s) {

        /**
         * When an Item were selected in automatically redirect to
         * the Editing page
         * */

        startActivity(new Intent(AdminEditingPage.this, EditPageForm.class));
        new EditPageForm(id, f, m, l, img, s);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AdminEditingPage.this, AdminPanel.class);
        startActivity(i);
        finish();
    }
}
