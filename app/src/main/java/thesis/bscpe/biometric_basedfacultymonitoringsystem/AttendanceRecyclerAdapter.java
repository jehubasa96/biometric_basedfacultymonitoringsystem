package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jehu Basa on 10/04/2017.
 */

public class AttendanceRecyclerAdapter extends RecyclerView.Adapter<AttendanceRecyclerAdapter.AttendanceVH>{

    private final LayoutInflater inflater;
    List<AttendanceRecordViewContent> list = new ArrayList<>();
    private AttendanceRecordViewContent content;

    public AttendanceRecyclerAdapter(Context c, List<AttendanceRecordViewContent>list){
        this.list = list;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public AttendanceRecyclerAdapter.AttendanceVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.attendance_rv_layout, parent, false);
        AttendanceVH holder = new AttendanceVH(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(AttendanceRecyclerAdapter.AttendanceVH holder, int position) {
        content = list.get(position);
        holder.name.setText(content.name);
        holder.attendance.setText(content.attendance);

        byte[] p = content.picture;
        Bitmap bm = BitmapFactory
                .decodeByteArray(p,0,p.length);
        holder.pp.setImageBitmap(bm);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class AttendanceVH extends RecyclerView.ViewHolder{
        ImageView pp;
        TextView name;
        TextView attendance;

        public AttendanceVH(View itemView) {
            super(itemView);

            pp = (ImageView)itemView.findViewById(R.id.profilePic);
            name = (TextView)itemView.findViewById(R.id.fullname);
            attendance = (TextView)itemView.findViewById(R.id.attendance);
        }
    }
}
