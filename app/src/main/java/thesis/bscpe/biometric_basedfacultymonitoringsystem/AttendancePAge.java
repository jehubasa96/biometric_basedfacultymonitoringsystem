package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;

public class AttendancePAge extends AppCompatActivity {

    Toolbar toolbar;
    private DatabaseHelper db;
    private RecyclerView recycler;
    private AttendanceRecyclerAdapter adapter;
    private LinearLayoutManager layoutmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Code for fullscreen Mode
        getWindow().addFlags(WindowManager.LayoutParams
                .FLAG_FULLSCREEN);

        setContentView(R.layout.activity_attendance_page);

        toolbar = (Toolbar)findViewById(R.id.toolbarAdminattendance);

        //Recycler View Codes
        db = new DatabaseHelper(this);
        recycler = (RecyclerView) findViewById(R.id.attendanceRecycler);
        adapter = new AttendanceRecyclerAdapter(this, db.getAttendance());
        recycler.setAdapter(adapter);
        layoutmanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        recycler.setLayoutManager(layoutmanager);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AttendancePAge.this, AdminPanel.class);
        startActivity(i);
        finish();
    }
}
