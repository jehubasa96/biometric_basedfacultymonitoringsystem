package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

public class AdminPanel extends AppCompatActivity {

    private Toolbar toolbarAdmin;
    private Button registerButton;
    private Button editDB;
    private ImageButton returnButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Code for fullscreen Mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_admin_panel);

        //ToolBar Code
        toolbarAdmin = (Toolbar) findViewById(R.id.toolbarAdmin);
        returnButton = (ImageButton) toolbarAdmin.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AdminPanel.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }


    /**
     * If the Edit Database Button were clicked it will be redirected
     * to AdminEditing page
     */
    public void onEditDBClick(View view) {
        startActivity(new Intent(this, AdminEditingPage.class));
        finish();
    }

    /**
     * If the Register Button were clicked it will be redirected
     * to FillUpForm page
     */
    public void onRegisterFormButtonClick(View view) {
        startActivity(new Intent(this, FillUpForm.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AdminPanel.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void onAttendnceClick(View view) {
        startActivity(new Intent(this, AttendancePAge.class));
        finish();
    }
}
