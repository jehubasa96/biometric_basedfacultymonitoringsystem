package thesis.bscpe.biometric_basedfacultymonitoringsystem;

/**
 * This class is one of the requirements of Recyclerview
 */

public class ViewingDBListContent {
    String FNAME, MNAME,LASTNAME, SCHED, STATUS;
    byte[] image;
    int id;

    public ViewingDBListContent(int id, String FNAME, String MNAME, String LASTNAME, String STATUS,
                                String SCHED, byte[] image) {
        this.FNAME = FNAME;
        this.MNAME = MNAME;
        this.LASTNAME = LASTNAME;
        this.SCHED = SCHED;
        this.STATUS = STATUS;
        this.image = image;
        this.id = id;
    }
}
