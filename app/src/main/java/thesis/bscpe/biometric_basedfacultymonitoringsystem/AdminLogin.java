package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

/**
 * This Class is for Admin Login Popup
 */

public class AdminLogin extends Dialog {

    public AdminLogin(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_login_panel_admin_tool);
    }
}
