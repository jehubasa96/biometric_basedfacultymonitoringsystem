package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private final String ACTION_USB_PERMISSION =
            "thesis.bscpe.biometric_basedfacultymonitoringsystem.USB_PERMISSION";

    private UsbManager usbManager;
    private UsbDevice device;
    private UsbDeviceConnection connection;
    private UsbSerialDevice serialPort;
    private TextView serialLog;
    private Handler handler;
    private Toolbar toolbar;
    private boolean pressTwice = false;
    private boolean connected = false;

    public static DatabaseHelper dbHelper;
    String idFP = "";
    boolean start = false;
    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = "";
            try {
                data = new String(arg0, "UTF-8");
                String startIndicator = "";
                if(start){
                    for(int x = 0; x<data.length(); x++){
                        startIndicator = data.substring(x,x+1);
                        if(startIndicator.equals(";")){
                            start = false;
                            showText(serialLog, "id: "+idFP);
                            changeStatus(idFP);
                            idFP = "";
                            break;
                        }
                        idFP += data.substring(x,x+1);
                    }
                }
                if(!start){
                    for(int x = 0; x<data.length(); x++){
                        startIndicator = data.substring(x,x+1);

                        if(start){
                            if(startIndicator.equals(";")){
                                start = false;
                                showText(serialLog, "id: "+idFP);
                                changeStatus(idFP);
                                idFP = "";
                                break;
                            }

                            idFP += data.substring(x,x+1);
                        }

                        if(startIndicator.equals("#")){
                            start = true;
                        }
                    }
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }
    };

    /**
     * The BroacastReciever is the one whose in-charge in listening to Arduino
     */
    private final BroadcastReceiver broadcastReceiver
            = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            showText(serialLog, "Serial Connection Opened!\n");

                            Handler h =new Handler();
                            h.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String cmd = "1";
                                    serialPort.write(cmd.getBytes());
                                }
                            },3000);

                            connected = true;
                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {

            }
        }
    };

    private void changeStatus(String idFP) {
        final int idfinger = Integer.parseInt(idFP);
        String newAttendance = "";
        Calendar c = Calendar.getInstance();
        String initAttendance = dbHelper.getAttendance(idfinger);

        final String name = dbHelper.getName(idfinger);
        boolean chk = dbHelper.getStatus(idfinger);
        if(name.equals("")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this,
                            "Unknown Fingerprint", Toast.LENGTH_SHORT).show();
                }
            });
        } else{
            if(chk){
                boolean stat = dbHelper.updateStatus(idfinger, "Out");

                if(stat){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Logged out successfully ("+
                                            name+")", Toast.LENGTH_SHORT).show();
                        }
                    });

                    newAttendance = initAttendance + "\n(Logged out) Date: "
                            +(c.get(Calendar.MONTH)+1)+"/"
                            +c.get(Calendar.DAY_OF_MONTH)+"/"
                            +c.get(Calendar.YEAR)+" Time: "
                            +c.get(Calendar.HOUR_OF_DAY)+":"
                            +c.get(Calendar.MINUTE);

                    dbHelper.updateAttendace(idfinger, newAttendance);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Logged out failed ("+
                                            name+")", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                boolean stat = dbHelper.updateStatus(idfinger, "In");

                if(stat){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Logged in successfully("+
                                            name+")", Toast.LENGTH_SHORT).show();
                        }
                    });

                    newAttendance = initAttendance + "\n(Logged in) Date: "
                            +(c.get(Calendar.MONTH)+1)+"/"
                            +c.get(Calendar.DAY_OF_MONTH)+"/"
                            +c.get(Calendar.YEAR)+" Time: "
                            +c.get(Calendar.HOUR_OF_DAY)+":"
                            +c.get(Calendar.MINUTE);

                    dbHelper.updateAttendace(idfinger, newAttendance);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Logged in failed("+
                                            name+")", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //for FullScreen Mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        Log.d("Main:", "Started");

        dbHelper = new DatabaseHelper(this);

        //Toolbar Code
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        serialLog = (TextView) findViewById(R.id.logArea);
        serialLog.setMovementMethod(new ScrollingMovementMethod());

        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);//initialize usbManager

        //Initialize BroadcastReciever
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        Toast.makeText(this, "Starting to listen", Toast.LENGTH_SHORT).show();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startToListen();
            }
        }, 3000);

    }

    public void startToListen() {
        showText(serialLog, "Starting to listen");
        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);
        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341)//Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this,
                            0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                    keep = false;
                    connected = true;
                } else {
                    connection = null;
                    device = null;
                }

                if (!keep)
                    break;
            }
        } else {
            Toast.makeText(this, "Cannot Detect Fingerprint Sensor", Toast.LENGTH_SHORT).show();
            //finish();
        }
    }

    public void logView(View v) {
        Intent i = new Intent(MainActivity.this, ViewLogBook.class);
        startActivity(i);
        finish();
    }

    public void onClickAdminTool(View v) {
        /**
         * Username and password for admintool button.
         * */
        final String username = "pogs";
        final String password = "1234";

        final AdminLogin login = new AdminLogin(this);
        login.requestWindowFeature(Window.FEATURE_NO_TITLE);
        login.show();

        final EditText usernameField = (EditText) login.findViewById(R.id.usernameAT);
        final EditText passwordField = (EditText) login.findViewById(R.id.passwordAT);
        Button ok = (Button) login.findViewById(R.id.adminTool_ok);
        Button cancel = (Button) login.findViewById(R.id.adminTool_cancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usernameField.getText().toString().equals(username) &&
                        passwordField.getText().toString().equals(password)) {

                    Intent i = new Intent(MainActivity.this, AdminPanel.class);
                    startActivity(i);
                    finish();
                    login.dismiss();

                } else {
                    login.dismiss();
                    Toast.makeText(MainActivity.this, "Wrong username or password",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.dismiss();
            }
        });

    }

    public void showText(TextView view, CharSequence text) {
        final TextView tView = view;
        final CharSequence mess = text;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tView.append(mess);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (pressTwice) {

            super.onBackPressed();
        }
        Toast.makeText(MainActivity.this, "Press back again to exit",
                Toast.LENGTH_SHORT).show();
        pressTwice = true;

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pressTwice = false;
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        if(connected){
            serialPort.write("0".getBytes());
            serialPort.close();
        }
    }
}
