package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class FingerPrintEnroll extends AppCompatActivity {
    private final String ACTION_USB_PERMISSION =
            "thesis.bscpe.biometric_basedfacultymonitoringsystem.USB_PERMISSION";

    private UsbManager usbManager;
    private UsbDevice device;
    private UsbDeviceConnection connection;
    private UsbSerialDevice serialPort;
    private TextView cmd;
    private boolean connected = false;

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = "";
            try {
                data += new String(arg0, "UTF-8");
                showText(cmd, data);

                for (int x = 0; x < data.length(); x++){
                    String temp = data.substring(x,x+1);
                    if(temp.equals("~")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(FingerPrintEnroll.this,
                                        "Successfully Registered", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Intent i = new Intent(FingerPrintEnroll.this, AdminPanel.class);
                        startActivity(i);
                        finish();
                    }else if(temp.equals("?")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder build = new AlertDialog.Builder(
                                        FingerPrintEnroll.this);
                                build.setTitle("Error")
                                        .setMessage("Fingerprint did not matched")
                                        .setCancelable(false)
                                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                final String dbId = String.valueOf(new DatabaseHelper(
                                                        FingerPrintEnroll.this)
                                                        .getIDNum());
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(FingerPrintEnroll.this, dbId
                                                                , Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                                serialPort.write(dbId
                                                        .getBytes());
                                                serialPort.write("\n"
                                                        .getBytes());
                                            }
                                        })
                                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                Intent i = new Intent(FingerPrintEnroll.this, AdminPanel.class);
                                                startActivity(i);
                                                finish();
                                            }
                                        });
                                AlertDialog alert = build.create();
                                alert.show();
                            }
                        });
                    }

                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    };

    private final BroadcastReceiver broadcastReceiver
            = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            Toast.makeText(FingerPrintEnroll.this,
                                    "Serial Open", Toast.LENGTH_SHORT).show();

                            Handler h = new Handler();
                            h.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    serialPort.write("2".getBytes());
                                }
                            }, 3000);

                            final String dbId = String.valueOf(new DatabaseHelper(
                                    FingerPrintEnroll.this)
                                    .getIDNum());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(FingerPrintEnroll.this, dbId
                                            , Toast.LENGTH_SHORT).show();
                                }
                            });

                            h.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    serialPort.write(dbId
                                            .getBytes());
                                    serialPort.write("\n"
                                            .getBytes());
                                }
                            }, 6000);
                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.finger_enroll_layout);
        cmd = (TextView) findViewById(R.id.cmdArea);
        cmd.setMovementMethod(new ScrollingMovementMethod());

        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        startToListen();
    }

    public void showText(TextView view, CharSequence text) {
        final TextView tView = view;
        CharSequence mess2 = text;
        final CharSequence mess = mess2;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tView.append(mess);
            }
        });
    }

    public void startToListen() {
        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);
        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341)//Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this,
                            0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                    keep = false;
                    connected = true;
                    Toast.makeText(this, "Detected", Toast.LENGTH_SHORT).show();
                } else {
                    connection = null;
                    device = null;
                }

                if (!keep)
                    break;
            }
        } else {
            Toast.makeText(this, "Cannot Detect Fingerprint Sensor", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(connected){
            serialPort.write("0".getBytes());
            serialPort.close();
        }
    }
}
