package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

public class ViewLogBook extends AppCompatActivity{
    private Toolbar toolbarViewLog;
    private ImageButton returnButton;
    private DatabaseHelper db;
    private RecyclerView recylerOnTravelViewer;
    private RecyclerView recylerInactiveViewer;
    private RecyclerView recylerActiveViewer;
    private OnTravelViewLogRecyclerAdapter OTadapter;
    private InactiveViewLogRecyclerAdapter Iadapter;
    private ActiveViewLogRecyclerAdapter Aadapter;
    private LinearLayoutManager active_layoutM;
    private LinearLayoutManager inactive_layoutM;
    private LinearLayoutManager ontravel_layoutM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_view_log_book);

        toolbarViewLog = (Toolbar) findViewById(R.id.toolbarViewLog);
        returnButton = (ImageButton) toolbarViewLog.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ViewLogBook.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        //RecyclerView(List) Initialization
        db = new DatabaseHelper(this);
        recylerActiveViewer = (RecyclerView) findViewById(R.id.activeRV);
        recylerInactiveViewer = (RecyclerView) findViewById(R.id.inactiveRV);
        recylerOnTravelViewer = (RecyclerView) findViewById(R.id.onTravelRV);
        Aadapter = new ActiveViewLogRecyclerAdapter(this, db.getAllActiveData());
        OTadapter = new OnTravelViewLogRecyclerAdapter(this, db.getAllOnTravelData());
        Iadapter = new InactiveViewLogRecyclerAdapter(this, db.getAllInactiveData());
        recylerActiveViewer.setAdapter(Aadapter);
        recylerInactiveViewer.setAdapter(Iadapter);
        recylerOnTravelViewer.setAdapter(OTadapter);
        active_layoutM = new LinearLayoutManager(this);
        inactive_layoutM = new LinearLayoutManager(this);
        ontravel_layoutM = new LinearLayoutManager(this);
        recylerActiveViewer.setLayoutManager(active_layoutM);
        recylerInactiveViewer.setLayoutManager(inactive_layoutM);
        recylerOnTravelViewer.setLayoutManager(ontravel_layoutM);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
