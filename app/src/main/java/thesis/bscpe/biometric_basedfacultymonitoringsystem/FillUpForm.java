package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class FillUpForm extends AppCompatActivity {

    private Spinner daysSpinner;
    private ArrayAdapter<CharSequence> adapter;
    private Toolbar toolbarFillup;
    private Button chooseID;

    final int REQUEST_CODE_GELLERY = 999;
    private ImageView profilePicture;

    private DatabaseHelper db;
    private EditText firstName;
    private EditText middleName;
    private EditText lastName;
    private EditText subjectSCH;
    private EditText roomSCH;
    private EditText timeSCH;
    private TextView schedule;
    private Button addSCH;
    private Toolbar toolbar;
    private ImageButton returnButton;

    String DAY_SELECTED = "";
    private String FNAME;
    private String MNAME;
    private String LNAME;
    private String SCHEDULE;
    String Arduino_Msg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FullSceen mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_fill_up_form);

        //Toolbar Code
        toolbar = (Toolbar) findViewById(R.id.toolbarFillUp);
        returnButton = (ImageButton) toolbar.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FillUpForm.this, AdminPanel.class);
                startActivity(i);
                finish();
            }
        });

        init();

        //Spinner(Dropdown List) initialization
        adapter = ArrayAdapter.createFromResource(this,
                R.array.weekdays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daysSpinner.setAdapter(adapter);
        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DAY_SELECTED = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                DAY_SELECTED = parent.toString();
            }
        });

    }

    public void init() {
        toolbarFillup = (Toolbar) findViewById(R.id.toolbarFillUp);
        daysSpinner = (Spinner) findViewById(R.id.daySpinnerFillUP);
        chooseID = (Button) findViewById(R.id.chooseIdButton);
        profilePicture = (ImageView) findViewById(R.id.profilePicImg);
        profilePicture.setImageBitmap(BitmapFactory.decodeResource(this.getResources(),
                R.drawable.iconmonstr_user_8_240));
        firstName = (EditText) findViewById(R.id.firstNameFillUp);
        middleName = (EditText) findViewById(R.id.middleNameFillUp);
        lastName = (EditText) findViewById(R.id.lastNameFillUp);
        subjectSCH = (EditText) findViewById(R.id.subjectFillUP);
        roomSCH = (EditText) findViewById(R.id.roomFillUP);
        timeSCH = (EditText) findViewById(R.id.timeFillUP);
        schedule = (TextView) findViewById(R.id.scheduleTextView);
        schedule.setMovementMethod(new ScrollingMovementMethod());

    }

    public void onChooseIDClick(View view) {
        ActivityCompat.requestPermissions(
                FillUpForm.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE_GELLERY
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GELLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, REQUEST_CODE_GELLERY);
            } else {
                Toast.makeText(this, "You don't have permission to access the file location!",
                        Toast.LENGTH_LONG).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GELLERY && resultCode == RESULT_OK &&
                data != null) {
            Uri uri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                profilePicture.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onFinish(View view) {
        FNAME = firstName.getText().toString().trim();
        MNAME = middleName.getText().toString().trim();
        LNAME = lastName.getText().toString().trim();
        SCHEDULE = schedule.getText().toString().trim();

        if(FNAME.equals("")&&MNAME.equals("")&&LNAME.equals("")
                &&SCHEDULE.equals("")){
            Toast.makeText(getBaseContext(), "Please fill up all fields",Toast.LENGTH_SHORT).show();
        } else {
            new SavingBGProcess().execute();
        }
    }

    class SavingBGProcess extends AsyncTask<String, Integer, String> {

        private boolean result;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            db = new DatabaseHelper(FillUpForm.this);
            Calendar c = Calendar.getInstance();

            String newAttendance = "(Created) Date: "
                    +(c.get(Calendar.MONTH)+1)+"/"
                    +c.get(Calendar.DAY_OF_MONTH)+"/"
                    +c.get(Calendar.YEAR)+" Time: "
                    +c.get(Calendar.HOUR_OF_DAY)+":"
                    +c.get(Calendar.MINUTE);

            result = db.insertData(imageToByte(profilePicture), FNAME,
                    MNAME, LNAME, SCHEDULE, newAttendance);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(FillUpForm.this, "Running in background",
                    "Processing\nPlease wait...");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (result) {
                Toast.makeText(FillUpForm.this, "Successfully added", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(FillUpForm.this, FingerPrintEnroll.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(FillUpForm.this, "Error in saving the data.", Toast.LENGTH_SHORT).
                        show();

                onBackPressed();
            }
        }
    }

    private byte[] imageToByte(ImageView img) {
        Bitmap bm = ((BitmapDrawable) img.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public void onAddSchedButton(View view) {
        if (subjectSCH.getText().toString().length() > 4 &&
                roomSCH.getText().toString().length() > 3 &&
                timeSCH.getText().toString().length() > 8) {

            final String getSchedule = DAY_SELECTED + ": \n" + subjectSCH.getText().toString() +
                    "|" + roomSCH.getText().toString() + "|" + timeSCH.getText().toString() + ";";

            //Monday: CS122|CE12|12:5-13:00;

            schedule.append(getSchedule + "\n\n");

            subjectSCH.setText("");
            roomSCH.setText("");
            timeSCH.setText("");
        } else {
            Toast.makeText(this, "Please fill the schedule field correctly!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(FillUpForm.this, AdminPanel.class);
        startActivity(i);
        finish();
    }
}
