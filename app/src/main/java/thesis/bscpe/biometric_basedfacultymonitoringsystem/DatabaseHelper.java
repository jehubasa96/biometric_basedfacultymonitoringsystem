package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * This Class is for the SQLite Database
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "Data.db";
    public static final String TABLE_NAME = "data_table";
    public static final String Col1 = "_id";
    public static final String Col2 = "Image";
    public static final String Col3 = "First_Name";
    public static final String Col4 = "Middle_Name";
    public static final String Col5 = "Last_Name";
    public static final String Col6 = "Status";
    public static final String Col7 = "Schedule";
    public static final String Col8 = "Attendance";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 2);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (" + Col1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Col2 + " BLOB, " + Col3 + " TEXT NOT NULL, " + Col4 + " TEXT NOT NULL, " +
                Col5 + " TEXT NOT NULL, " + Col6 + " TEXT NOT NULL DEFAULT 'Out', " +
                Col7 + " TEXT NOT NULL, "+
                Col8 + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TAABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(byte[] img, String f, String m, String l, String sch, String att) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Col2, img);
        contentValues.put(Col3, f);
        contentValues.put(Col4, m);
        contentValues.put(Col5, l);
        contentValues.put(Col7, sch);
        contentValues.put(Col8, att);

        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public List<EditingDBListContent> getData() {
        List<EditingDBListContent> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            EditingDBListContent content = new EditingDBListContent(c.getInt(0),
                    c.getString(2), c.getString(3), c.getString(4), c.getString(6),
                    c.getBlob(1));
            list.add(content);

            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    public List<ViewingDBListContent> getAllActiveData() {
        List<ViewingDBListContent> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {

            if (c.getString(5).equals("In")) {
                ViewingDBListContent content = new ViewingDBListContent(c.getInt(0),
                        c.getString(2), c.getString(3), c.getString(4), c.getString(5),
                        c.getString(6), c.getBlob(1));
                list.add(content);
            }
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    public List<ViewingDBListContent> getAllInactiveData() {
        List<ViewingDBListContent> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {

            if (c.getString(5).equals("Out")) {
                ViewingDBListContent content = new ViewingDBListContent(c.getInt(0),
                        c.getString(2), c.getString(3), c.getString(4), c.getString(5),
                        c.getString(6), c.getBlob(1));
                list.add(content);
            }
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    public List<ViewingDBListContent> getAllOnTravelData() {
        List<ViewingDBListContent> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {

            if (c.getString(5).equals("On Travel")) {
                ViewingDBListContent content = new ViewingDBListContent(c.getInt(0),
                        c.getString(2), c.getString(3), c.getString(4), c.getString(5),
                        c.getString(6), c.getBlob(1));
                list.add(content);
            }
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    public List<AttendanceRecordViewContent> getAttendance(){
        List<AttendanceRecordViewContent> list = new ArrayList<>();
        SQLiteDatabase db =getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while(!c.isAfterLast()){

            AttendanceRecordViewContent a = new AttendanceRecordViewContent(c.getBlob(1),
                    c.getString(2), c.getString(3), c.getString(4),c.getString(7));
            list.add(a);

            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    public boolean updateData(int id, byte[] img, String f, String m, String l, String st,
                              String sch) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1, id);
        contentValues.put(Col2, img);
        contentValues.put(Col3, f);
        contentValues.put(Col4, m);
        contentValues.put(Col5, l);
        contentValues.put(Col6, st);
        contentValues.put(Col7, sch);
        db.update(TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public boolean updateStatus(int id, String st) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1, id);
        contentValues.put(Col6, st);
        db.update(TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public boolean updateAttendace(int id, String at) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1, id);
        contentValues.put(Col8, at);
        db.update(TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(id)});
        return true;
    }

    public Integer deleteData(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_NAME, "_id = ?", new String[]{String.valueOf(id)});
    }

    public Integer getIDNum(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToLast();
        return c.getInt(0);
    }

    public boolean getStatus(int ID){
        SQLiteDatabase db = getReadableDatabase();
        boolean st = false;
        Cursor c =db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            if(ID == c.getInt(0)){
                if(c.getString(5).equals("In")){
                    st = true;
                    break;
                }
            }
            c.moveToNext();
        }
        return st;
    }

    public String getName(int ID){
        SQLiteDatabase db = getReadableDatabase();
        String name = "";
        Cursor c =db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            if(ID == c.getInt(0)){
                name = c.getString(4) +", "+c.getString(2)+" "+c.getString(3);
                break;
            }
            c.moveToNext();
        }
        return name;
    }

    public String getAttendance(int ID){
        SQLiteDatabase db = getReadableDatabase();
        String name = "";
        Cursor c =db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            if(ID == c.getInt(0)){
                name = c.getString(7);
                break;
            }
            c.moveToNext();
        }
        return name;
    }
}
