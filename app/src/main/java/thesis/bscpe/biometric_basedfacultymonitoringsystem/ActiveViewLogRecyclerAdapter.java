package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is for Active Faculties' recyclerview adapter
 */

public class ActiveViewLogRecyclerAdapter extends RecyclerView.Adapter<ActiveViewLogRecyclerAdapter
        .ViewLogRecyclerAdapterVH> {

    private final LayoutInflater inflater;
    List<ViewingDBListContent> list = new ArrayList<>();
    private ViewingDBListContent contents;
    Context c;

    public ActiveViewLogRecyclerAdapter(Context c, List<ViewingDBListContent> list) {
        this.c = c;
        inflater = LayoutInflater.from(c);
        this.list = list;
    }

    @Override
    public ActiveViewLogRecyclerAdapter.ViewLogRecyclerAdapterVH onCreateViewHolder(ViewGroup parent,
                                                                                    int viewType) {
        View v = inflater.inflate(R.layout.viewlog_rv_layout, parent, false);
        ViewLogRecyclerAdapterVH holder = new ViewLogRecyclerAdapterVH(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ActiveViewLogRecyclerAdapter.ViewLogRecyclerAdapterVH holder,
                                 int position) {
        contents = list.get(position);
        holder.fullname.setText(contents.LASTNAME + ", " + contents.FNAME + " " + contents.MNAME);
        holder.fingerprintID.setText("Fingerprint ID#: " + contents.id);
        holder.schedule.setText(contents.SCHED);
        holder.status.setText("Status: " + contents.STATUS);

        byte[] pp = contents.image;
        Bitmap bitmap = BitmapFactory.decodeByteArray(pp, 0, pp.length);
        holder.picture.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewLogRecyclerAdapterVH extends RecyclerView.ViewHolder  {
        ImageView picture;
        TextView fullname, fingerprintID, schedule, status;

        public ViewLogRecyclerAdapterVH(View itemView) {
            super(itemView);

            picture = (ImageView) itemView.findViewById(R.id.viewRecycler_ProfilePic);
            fullname = (TextView) itemView.findViewById(R.id.viewRecycler_fullname);
            fingerprintID = (TextView) itemView.findViewById(R.id.viewRecycler_FingerprintIdNum);
            schedule = (TextView) itemView.findViewById(R.id.viewRecycler_Schedule);
            status = (TextView) itemView.findViewById(R.id.viewRecycler_StatusView);
        }

    }
}
