package thesis.bscpe.biometric_basedfacultymonitoringsystem;


public class AttendanceRecordViewContent {
    byte[] picture;
    String name;
    String attendance;

    public AttendanceRecordViewContent(byte[] picture, String f, String m, String l,
                                       String schedule) {
        this.picture = picture;
        this.name = l+", "+f+" "+m;
        attendance = schedule;
    }
}
