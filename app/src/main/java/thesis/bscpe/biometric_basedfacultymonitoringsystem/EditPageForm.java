package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class EditPageForm extends AppCompatActivity {

    private Spinner daysSpinner;
    private ArrayAdapter<CharSequence> adapter;

    final int REQUEST_CODE_GELLERY = 999;
    ImageView profilePicture;

    private DatabaseHelper db;
    EditText firstName;
    EditText middleName;
    EditText lastName;
    EditText subjectSCH;
    EditText roomSCH;
    EditText timeSCH;
    TextView schedule;
    Toolbar toolbar;
    ImageButton returnButton;

    String DAY_SELECTED = "";
    String STATUS_SELECTED = "";
    private String FNAME;
    private String MNAME;
    private String LNAME;
    private String SCHEDULE;
    static String IFNAME;
    static String IMNAME;
    static String ILNAME;
    static String SCHED;
    static byte[] IMG;
    static int id;

    private Spinner status;
    private ArrayAdapter<CharSequence> statusAdapter;


    public EditPageForm(int id, String f, String m, String l, byte[] img, String s) {
        this.id = id;
        this.IFNAME = f;
        this.IMNAME = m;
        this.ILNAME = l;
        this.IMG = img;
        this.SCHED = s;
        Log.d("Check:", f + m + l + s);
    }

    public EditPageForm() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //code for FullScreen mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_edit_page_form);

        //Toolbar initialization
        toolbar = (Toolbar) findViewById(R.id.toolbarEditPage);
        returnButton = (ImageButton) toolbar.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditPageForm.this, AdminPanel.class);
                startActivity(i);
                finish();
            }
        });

        init();

        //Spinner(Dropdown List) Initialization
        adapter = ArrayAdapter.createFromResource(this,
                R.array.weekdays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daysSpinner.setAdapter(adapter);
        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DAY_SELECTED = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                DAY_SELECTED = parent.toString();
            }
        });

        statusAdapter = ArrayAdapter.createFromResource(this,
                R.array.status, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status.setAdapter(statusAdapter);
        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                STATUS_SELECTED = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                STATUS_SELECTED = parent.toString();
            }
        });
    }

    public void init() {
        status = (Spinner) findViewById(R.id.status);
        daysSpinner = (Spinner) findViewById(R.id.daySpinnerFillUP);
        profilePicture = (ImageView) findViewById(R.id.profilePicImg);
        profilePicture.setImageBitmap(BitmapFactory.decodeByteArray(IMG, 0, IMG.length));
        firstName = (EditText) findViewById(R.id.firstNameFillUp);
        firstName.setText(IFNAME.toString());
        middleName = (EditText) findViewById(R.id.middleNameFillUp);
        middleName.setText(IMNAME.toString());
        lastName = (EditText) findViewById(R.id.lastNameFillUp);
        lastName.setText(ILNAME.toString());
        subjectSCH = (EditText) findViewById(R.id.subjectFillUP);
        roomSCH = (EditText) findViewById(R.id.roomFillUP);
        timeSCH = (EditText) findViewById(R.id.timeFillUP);
        schedule = (TextView) findViewById(R.id.scheduleTextView);
        schedule.setText(SCHED.toString());
        schedule.setMovementMethod(new ScrollingMovementMethod());
    }

    public void onChooseIDClick(View view) {
        ActivityCompat.requestPermissions(
                EditPageForm.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_CODE_GELLERY
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GELLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, REQUEST_CODE_GELLERY);
            } else {
                Toast.makeText(this, "You don't have permission to access the file location!",
                        Toast.LENGTH_LONG).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GELLERY && resultCode == RESULT_OK &&
                data != null) {
            Uri uri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                profilePicture.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onSave(View view) {
        FNAME = firstName.getText().toString().trim();
        MNAME = middleName.getText().toString().trim();
        LNAME = lastName.getText().toString().trim();
        SCHEDULE = schedule.getText().toString().trim();

        new EditPageForm.SavingBGProcess().execute();
    }

    public void onDelete(View view) {
        db = new DatabaseHelper(EditPageForm.this);
        Integer result = db.deleteData(id);
        if (result > 0) {
            Toast.makeText(EditPageForm.this, "Successfully deleted", Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(EditPageForm.this, "Error in deleting the data.", Toast.LENGTH_SHORT).
                    show();
            onBackPressed();
        }
    }

    public void onClear(View view) {
        schedule.setText("");
    }


    class SavingBGProcess extends AsyncTask<String, Integer, String> {

        private boolean result;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            db = new DatabaseHelper(EditPageForm.this);
            result = db.updateData(id, imageToByte(profilePicture), FNAME,
                    MNAME, LNAME, STATUS_SELECTED, SCHEDULE);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(EditPageForm.this, "Running in background",
                    "Saving your data\nPlease wait...");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (result) {
                Toast.makeText(EditPageForm.this, "Successfully added", Toast.LENGTH_SHORT).show();
                onBackPressed();
            } else {
                Toast.makeText(EditPageForm.this, "Error in saving the data.", Toast.LENGTH_SHORT).
                        show();
                onBackPressed();
            }
            db.close();
        }
    }

    private byte[] imageToByte(ImageView img) {
        Bitmap bm = ((BitmapDrawable) img.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public void onAddSchedButton(View view) {
        if (subjectSCH.getText().toString().length() > 4 &&
                roomSCH.getText().toString().length() > 3 &&
                timeSCH.getText().toString().length() > 8) {

            final String getSchedule = DAY_SELECTED + ": \n" + subjectSCH.getText().toString() +
                    "|" + roomSCH.getText().toString() + "|" + timeSCH.getText().toString() + ";";

            //Monday: CS122|CE12|12:5-13:00;

            schedule.append(getSchedule + "\n\n");

            subjectSCH.setText("");
            roomSCH.setText("");
            timeSCH.setText("");

        } else {
            Toast.makeText(this, "Please fill the schedule field correctly!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(EditPageForm.this, AdminEditingPage.class);
        startActivity(i);
        finish();
    }
}
