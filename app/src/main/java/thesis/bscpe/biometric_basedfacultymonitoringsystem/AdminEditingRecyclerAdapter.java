package thesis.bscpe.biometric_basedfacultymonitoringsystem;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is for AdminEditing's recyclerview adapter
 */

public class AdminEditingRecyclerAdapter extends RecyclerView.Adapter<AdminEditingRecyclerAdapter.
        AdminEditingRecyclerAdapterVH> {

    private final LayoutInflater inflater;
    List<EditingDBListContent> list = new ArrayList<>();
    private EditingDBListContent contents;
    Context c;
    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public AdminEditingRecyclerAdapter(Context c, List<EditingDBListContent> list) {
        this.list = list;
        inflater = LayoutInflater.from(c);
        this.c = c;
    }

    @Override
    public AdminEditingRecyclerAdapter.
            AdminEditingRecyclerAdapterVH onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = inflater.inflate(R.layout.editing_rv_layout, parent, false);
        AdminEditingRecyclerAdapterVH holder = new AdminEditingRecyclerAdapterVH(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(AdminEditingRecyclerAdapter.
                                         AdminEditingRecyclerAdapterVH holder, int position) {
        contents = list.get(position);
        holder.fullname.setText(contents.LASTNAME + ", " + contents.FNAME + " " + contents.MNAME);
        holder.fingerprintID.setText("Fingerprint ID#: " + contents.id);
        holder.schedule.setText(contents.SCHED);
        holder.fn = contents.FNAME;
        holder.mn = contents.MNAME;
        holder.ln = contents.LASTNAME;
        holder.img = contents.image;
        holder.id = contents.id;
        holder.s = contents.SCHED;

        byte[] pp = contents.image;
        Bitmap bitmap = BitmapFactory.decodeByteArray(pp, 0, pp.length);
        holder.picture.setImageBitmap(bitmap);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClick {
        void item(int id, String f, String m, String l, byte[] img, String s);
    }

    class AdminEditingRecyclerAdapterVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView picture;
        TextView fullname, fingerprintID, schedule;
        String fn, mn, ln,s;
        int id;
        byte[] img;

        public AdminEditingRecyclerAdapterVH(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            picture = (ImageView) itemView.findViewById(R.id.editRecycler_ProfilePic);
            fullname = (TextView) itemView.findViewById(R.id.editRecycler_fullname);
            fingerprintID = (TextView) itemView.findViewById(R.id.editRecycler_FingerprintIdNum);
            schedule = (TextView) itemView.findViewById(R.id.editRecycler_Schedule);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(c, "Clicked", Toast.LENGTH_SHORT).show();
            onItemClick.item(id, fn, mn, ln, img, s);
        }
    }
}
